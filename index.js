//Import thư viện expressjs tương đương import express from "express";
const express = require("express");

// Import thư viện mongoose js
const mongoose = require("mongoose");

//Khởi tạo 1 app express
const app = express();

// khai báo cổng chạy project
const port = 8000;

// Import router Module
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

// Để đọc đươc body json
app.use(express.json());

app.use((req, res, next) => {
  let today = new Date();
  console.log("Current: ", today);
  next();
});

app.use((req, res, next) => {
  console.log(req.method);
  next();
});

mongoose.connect("mongodb://localhost:27017/CRUD_Course", (error) => {
  if (error) throw error;

  console.log("Connect successfully!");
});

// Callback function là 1 function đóng vai trò là tham số của 1 func khác, nó sẽ dc thực hiện khi func chủ dc gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
  let today = new Date();

  res.json({
    message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${
      today.getMonth() + 1
    } năm ${today.getFullYear()}`,
  });
});

app.use(courseRouter);
app.use(reviewRouter);
app.listen(port, () => {
  console.log("App listening on port: ", port);
});
