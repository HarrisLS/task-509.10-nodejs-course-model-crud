// Import thư viện mongoose
const mongoose = require("mongoose");

// Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

// Khai báo review schema
const reviewSchema = new Schema(
  {
    // _id  có thể khai báo or ko
    _id: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
    stars: {
      type: Number,
      default: 0,
    },
    note: {
      type: String,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("reviews", reviewSchema);
