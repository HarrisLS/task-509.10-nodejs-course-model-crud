// Import thư viện mongoose
const { response } = require("express");
const mongoose = require("mongoose");

// Import Course Model
const courseModel = require("../models/courseModel");

// Create course
const createCourse = (req, res) => {
  //B1: Thu thập dữ liệu từ req
  let body = req.body;
  console.log(body);

  //B2: Validate dữ liệu
  if (!body.bodyTitle) {
    return res.status(400).json({
      message: "Title is required!",
    });
  }

  if (!Number.isInteger(body.bodyNoStudent) || body.bodyNoStudent < 0) {
    return res.status(400).json({
      message: "No Student is invalid!",
    });
  }

  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newCourseData = {
    _id: mongoose.Types.ObjectId(),
    title: body.bodyTitle,
    description: body.bodyDescription,
    noStudent: body.bodyNoStudent,
  };
  courseModel.create(newCourseData, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(201).json({
      message: "Create successfully!",
      newCourse: data,
    });
  });
};

// Get all course
const getAllCourse = (req, res) => {
  //B3
  courseModel.find((error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Get all course successfully!",
      courses: data,
    });
  });
};

// GET course by id
const getCourseById = (req, res) => {
  //B1
  let courseId = req.params.courseId;

  //B2
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    return res.status(400).json({
      message: "Couesr ID is invalid!",
    });
  }
  //B3
  courseModel.findById(courseId, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Get course by ID successfully!",
      course: data,
    });
  });
};

// Update course by id
const updateCourseById = (req, res) => {
  // B1
  let courseId = req.params.courseId;
  let body = req.body;

  //B2
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    return res.status(400).json({
      message: "Couesr ID is invalid!",
    });
  }
  // Bóc tách trường hợp undefied
  if (body.bodyTitle !== undefined && body.bodyTitle == "") {
    return res.status(400).json({
      message: "Title is required!",
    });
  }

  if (
    body.bodyNoStudent !== undefined &&
    (!Number.isInteger(body.bodyNoStudent) || body.bodyNoStudent < 0)
  ) {
    return res.status(400).json({
      message: "No Student is invalid!",
    });
  }
  // B3
  let courseUpdate = {
    title: body.bodyTitle,
    description: body.bodyDescription,
    noStudent: body.bodyNoStudent,
  };

  courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Update course by ID successfully!",
      course: data,
    });
  });
};

// Delete course by id
const deleteCourseById = (req, res) => {
  //B1
  let courseId = req.params.courseId;
  //B2
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    return res.status(400).json({
      message: "Couesr ID is invalid!",
    });
  }
  //B3
  courseModel.findByIdAndDelete(courseId, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Delete course by ID successfully!",
    });
  });
};

// Export Course controller thành 1 module

module.exports = {
  createCourse,
  getAllCourse,
  getCourseById,
  updateCourseById,
  deleteCourseById,
};
